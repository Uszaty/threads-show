import java.time.Instant;

class Bufor extends Thread {
	private int contents;
	private boolean available = false;

	public Bufor() {
	}

	synchronized int get() {
		while (!available) {
			try {
				wait();
			} catch (InterruptedException e) {
				System.out.println("Interrupted");
			}
		}
		available = false;
		notifyAll();
		return contents;
	}

	synchronized void put(int value) {
		while (available) {
			try {
				wait();
			} catch (InterruptedException e) {
				System.out.println("Interrupted");
			}
		}
		System.out.println("Putting - " + value);
		contents = value;
		available = true;
		notifyAll();
	}

	public void run() {
		while (true) {
			System.out.println("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tcontents - " + contents + "  " + Instant.now().toString());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.out.println("Buffor refreshing status exception");
			}
		}
	}

}
