public class Main {
	private static final int SLEEP_TIME = 1000;

	public static void main(String[] args) throws Exception {
		Bufor bufor = new Bufor();
		Producent p1 = new Producent(bufor, 1);
		Producent p2 = new Producent(bufor, 2);
		Producent p3 = new Producent(bufor, 3);
		Producent p4 = new Producent(bufor, 4);
		Producent p5 = new Producent(bufor, 5);

		Konsument konsument = new Konsument(bufor, 1);

		bufor.start();

		p1.start();
		p2.start();
		p3.start();
		p4.start();
		p5.start();

		konsument.start();

	}
}
