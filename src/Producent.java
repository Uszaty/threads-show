public class Producent extends Thread {
	private Bufor buf;
	private int number;

	public Producent(Bufor bufor, int number) {
		buf = bufor;
		this.number = number;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			try {
				sleep((int) (Math.random() * 10000));
			} catch (InterruptedException e) {
			}
			System.out.println("Producent #" + this.number + " put: " + i);
			buf.put(i*number);
		}
	}
}
