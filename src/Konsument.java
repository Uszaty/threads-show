public class Konsument extends Thread {
	private Bufor buf;
	private int number;

	public Konsument(Bufor bufor, int number) {
		buf = bufor;
		this.number = number;
	}

	public void run() {
		int value = 0;
		for (;;) {
			value = buf.get();
			System.out.println("    Konsument #" +
					this.number + " got: " + value);
		}
	}
}
